INTRODUCTION:

Created a perfect pythonic solution for performing various functions on a set of videos . Various functions are performed for preprocessing of videos present in a single folder.
The functions used in preprocessing are rotating frames of videos by 90 degree, converting colored frames to grayscale, logging the FPS(Frames Per Second), 
using command line arguments to take the input video, and saving the output video.

ENVIRONMENT SETUP:
 
For running this pythonic solution, the following requirements should be fulfilled:
a. Python 3.7.9
b. opencv library: Installed using pip install opencv-python
NOTE*: sys, time, glob library installed automatically with python
sys : It provides various functions and variables that are used to manipulate different parts of the Python runtime environment.
time: It provides many ways of representing time in code, such as objects, numbers, and strings. 
glob : It is used to return all file paths that match a specific pattern.

APPROACH:

a. My first target was to create a simple python code for preprocessing of a single video. For this, I first take the video as input using VideoCapture() function. 
After this, I applied cv2.cvtColor() function for converting BGR video frames into grayscale. Then, applied cv2.rotate() function for rotating the video frames by 90 degree. 
Now, for calculating the fps of video, I subtract previous frame time from new frame time to get accurate results. Then fps is calculated using formula:
fps = 1/(new_frame_time-prev_frame_time)
After apply all the above functions, my next task to create a dataset of videos.

b. I downloaded some videos from google and save them to a single folder.  After this , I have given list of all the video formats that can be present in any dataset. 
Then I filter the video formats that are present in my dataset. Then , I run a 'for' loop for doing all the operations on every video present in dataset.

c. For saving the preprocessed output videos, I use VideoWriter() function.

d. After all the above steps, I set command line arguments to take input video and  use command line flags to set each of the 3 (gray scale conversion, rotating frames by 90 degree ,
    and saving output video) functionalities as optional feature. I passed arguments as follows:
    1. gs = grayscale
    2. r9 = rotate frame by 90 degree
    3. nv = saving output video as new video
Then I set these variables to use command line arguments.

INPUT FORMAT:

a. In the code, I give input a dataset folder whcih have some videos and some unwanted images & text files. I put some unwanted dataset in folder to increase the efficiency of my code, 
so that it can filter out all the video files from that folder.
Supported input video formats are '.flv', '.f4v', '.f4p', '.f4a', '.f4b','.nsv','.webm','.mkv','.flv','.vob','.ogv', '.ogg','.drc','.gif','.gifv','.mng','.avi','.mts','.m2ts','.ts','.mov','.qt','.wmv','.yuv','.roq',
'.mxf','.3g2','.3gp','.svi','.m4v','.mpg','.mpeg','.m2v','.mp2','.mpe','.mpv','.mp4','.m4p','.m4v','.amv','.asf','.viv','.rmvb','.rm'

b.First, I open the command prompt terminal in the folder containg code. After opening first type python for setting the environment, then give space then type the name of python
 script and after giving space put the path of video dataset in inverted comma , and at last pass the arguments gs, r9, nv.
  PS C:\Users\rittu\Downloads\code> python .\BasicCVsystem_Q2.py 'C:\Users\rittu\Downloads\video dataset' gs r9 nv
